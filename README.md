# README #

This app is a simple photo viewer app inspired by the wallpaper finder apps that are available in the app store.

App is implemented using **Swift 3.**

* This app is a photo viewer app that fetches photos from Flickr via Flickr's search API. 

Functions available:

- Searching of photos / tags (from Flickr) via search bar 
- Selection of photos in the collection view
- Viewing of larger images within the Detail View
- Traversal from previous to next photos within the Detail View

### Dependencies, References, Libraries ###

* PKHUD - https://github.com/pkluz/PKHUD. A convenient heads up display library for providing feedback to users while data is fetched and loaded on-screen

* KingFisher - https://github.com/onevcat/Kingfisher. A lightweight image processing library used for asynchronous downloading and rendering of images

* Flickr API Caller by RazeWare - code snippet for calling the Flickr API and return custom Flickr models

* Tutorial for UICollectionView by Ray Wenderlich - https://www.raywenderlich.com/136159/uicollectionview-tutorial-getting-started. The reference/inspiration for creating the UICollectionView approach.


### Modifications of existing libraries ###

RazeWare's Flickr API has been modified to only take care of the creation and fetching of the Flickr API calls. Even if this API also includes a download mechanism, it was not used in line with using KingFisher's asynchronous download and image processing capabilities

### How to Setup and Run ###

1. Clone repository
2. Open the "PhotoViewer.xcworkspace" (xcworkspace, and NOT xcodeproj)
3. For running in simulator, just simply press Run
4. For running in an actual device, you may have to modify the project properties to match your existing provisioning profiles.

### Caveats ###

Normally, pods are not included within the repository, and repository viewers (or downloaders), will be instructed to run pod install. 

However, since this app is for coding exam purposes, the pods have been also committed to the repository so that no extra configuration steps will be needed.