//
//  PhotoCellCollectionViewCell.swift
//  PhotoViewer
//
//  Created by Jacquiline Guzman on 2017/07/01.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class PhotoCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
