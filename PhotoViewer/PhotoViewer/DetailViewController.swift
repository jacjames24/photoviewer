//
//  DetailViewController.swift
//  PhotoViewer
//
//  Created by Jacquiline Guzman on 2017/07/01.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit
import Kingfisher
import PKHUD

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomBar: UIView!
    
    var photo:FlickrPhoto?
    var searches = [FlickrSearchResults]()
    var currentIndexOfSelectedPhoto:IndexPath?
    var currentIndex = 0
    var photosArray = [FlickrPhoto]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currentIndexOfSelectedPhoto != nil {
            loadPhoto()
        }
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(DetailViewController.tapDetected))
        singleTap.numberOfTapsRequired = 1
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
        
        let exitTap = UITapGestureRecognizer(target: self, action: #selector(DetailViewController.dismissSelf))
        exitTap.numberOfTapsRequired = 1
        bottomBar.isUserInteractionEnabled = true
        bottomBar.addGestureRecognizer(exitTap)
        
        //swipe right gesture will go to the previous day(from left to right)
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(leftAction))
        swipeRightGesture.direction = UISwipeGestureRecognizerDirection.right
        imageView.addGestureRecognizer(swipeRightGesture)
        
        //swipe left gesture will go to the next day (from right to left)
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(rightAction))
        swipeLeftGesture.direction = UISwipeGestureRecognizerDirection.left
        imageView.addGestureRecognizer(swipeLeftGesture)
        
        singleTap.cancelsTouchesInView = false
        exitTap.cancelsTouchesInView = false
        
    }
    
    func leftAction() {
        print("should go to the previous picture")
        
        if let index = currentIndexOfSelectedPhoto?.row {
            print("indexPath = \(index)")
            
            if (currentIndexOfSelectedPhoto?.row)! > 0 {
                currentIndexOfSelectedPhoto?.row = (currentIndexOfSelectedPhoto?.row)! - 1
            }
        }
        
        loadPhoto()
    }
    
    func rightAction() {
        print("should go to the next picture")
        
        if let index = currentIndexOfSelectedPhoto {
            print("indexPath = \(index)")
            
            if (currentIndexOfSelectedPhoto?.row)! < searches[index.section].searchResults.count - 1 {
                currentIndexOfSelectedPhoto?.row = (currentIndexOfSelectedPhoto?.row)! + 1
            }
        }
        
        loadPhoto()
    }
    
    func loadPhoto(){
        if let index = currentIndexOfSelectedPhoto {
            let photo = photoForIndexPath(indexPath: index)
            
            imageView.kf.setImage(with: photo.flickrLargeImageURL(),
                                  placeholder: nil,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
                                    print("\(receivedSize)/\(totalSize)")
            },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    print("Finished")
            })
        }
    }
    
    func photoForIndexPath(indexPath: IndexPath) -> FlickrPhoto{
        return searches[(indexPath as NSIndexPath).section].searchResults[(indexPath as IndexPath).row]
    }

    
    func tapDetected() {
        print("Imageview Clicked")
    }
    
    func dismissSelf(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func moveToRight(){
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
