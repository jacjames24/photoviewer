//
//  PhotosViewController.swift
//  PhotoViewer
//
//  Created by Jacquiline Guzman on 2017/06/27.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit
import PKHUD
import Kingfisher

class PhotosViewController: UICollectionViewController {
    fileprivate let reuseIndentifier = "PhotoCell"
    fileprivate let sectionInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)
    
    fileprivate var searches = [FlickrSearchResults]()
    fileprivate var flickr = Flickr()
    fileprivate let itemsPerRow: CGFloat = 3
    
    var currentPage = 1
    var isCurrentlyDownloading = false
    var itemToSearch = "architecture"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("in the prepare for segue")
        
        if let indexPath = getIndexPathForSelectedCell() {
            print("there is an indexpath, in the prepare for segue")
            
            let photo = photoForIndexPath(indexPath: indexPath)
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.photo = photo
            detailViewController.currentIndexOfSelectedPhoto = indexPath
            detailViewController.searches = searches
        }
    }
    
    func getIndexPathForSelectedCell() -> IndexPath? {
        var indexPath:IndexPath?
        
        if (self.collectionView?.indexPathsForSelectedItems?.count)! > 0{
            indexPath = self.collectionView?.indexPathsForSelectedItems?[0]
        }
        
        return indexPath!
    }
    
    override func viewDidLoad() {
        isCurrentlyDownloading = true
        searchForPhotos(termToSearch: itemToSearch, page:currentPage)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PhotosViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    

    func rotated() {
        self.collectionView?.reloadData()
    }
    
    func searchForPhotos(termToSearch:String, page:Int?) {
        var currentPage = 1
        
        if page != nil {
            currentPage = page!
        }else {
            //this is the first page, so collectionview is empty
            HUD.show(.progress)
        }
        
        flickr.searchFlickrForTerm(termToSearch, page: currentPage) { (results, error) in
            if let error = error {
                print("There is an error with searching = \(error)")
                
                if currentPage == 1 {
                    HUD.flash(.error, delay: 1.0)
                }
                
                self.isCurrentlyDownloading = false
                return
            }
            
            if let results = results {
                print("Found  \(results.searchResults.count) results for \(results.searchTerm)")
                
                self.searches.append(results)
                
                if currentPage == 1 {
                    HUD.flash(.success, delay: 1.0)
                }
                
                self.isCurrentlyDownloading = false
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
                
            }
        }
    }

}

private extension PhotosViewController{
    
    //get the specific photo
    func photoForIndexPath(indexPath: IndexPath) -> FlickrPhoto{
        return searches[(indexPath as NSIndexPath).section].searchResults[(indexPath as IndexPath).row]
    }
}

extension PhotosViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //clear all values to accomodate new results
        //and set to currentPage = 1
        
        self.itemToSearch = textField.text!
        
        self.searches.removeAll()
        self.currentPage = 1
        
        searchForPhotos(termToSearch: itemToSearch, page:currentPage)
        textField.resignFirstResponder()
        
        return true
    }
}

extension PhotosViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return searches.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searches[section].searchResults.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIndentifier, for: indexPath) as! PhotoCellCollectionViewCell
        
        cell.imageView.kf.indicatorType = .activity
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let photo:FlickrPhoto? = photoForIndexPath(indexPath: indexPath)
        
        _ = (cell as! PhotoCellCollectionViewCell).imageView.kf.setImage(with: photo?.flickrImageURL(),
                                                                         placeholder: nil,
                                                                         options: [.transition(ImageTransition.fade(1))],
                                                                         progressBlock: { receivedSize, totalSize in
                                                                            print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
        },
                                                                         completionHandler: { image, error, cacheType, imageURL in
                                                                            print("\(indexPath.row + 1): Finished")
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "gotoDetail", sender: self)
    }
    
    
}

extension PhotosViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        //square sized photo
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewBottomOffset = scrollView.contentSize.height
        
        if scrollView.contentOffset.y + scrollView.frame.size.height > scrollViewBottomOffset {
            //for downloading next batch of images
            
            if isCurrentlyDownloading == false {
                isCurrentlyDownloading = true
                self.currentPage = currentPage + 1
                searchForPhotos(termToSearch: self.itemToSearch, page:currentPage)
            }
        }
    }
}
